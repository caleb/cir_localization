#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Syncronizaiton
# Author: Kevin
# GNU Radio version: 3.8.0.0

from distutils.version import StrictVersion
from cir_rx8d.py import cir_rx8d.py

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
import time
from gnuradio import uhd
import signal

###################################
# Timing function added by Kevin. This function allows the user specify the radio start time and resets the pps on the radio for syncronization. 
# Code based on code from:
# http://lists.ettus.com/pipermail/usrp-users_lists.ettus.com/attachments/20150115/66ac59c6/attachment-0002.html
####################################
def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-a", "--alpha", dest="alpha", type=eng_float, default="500.0m",
        help="Set alpha [default=%(default)r]")
    parser.add_argument(
        "-f", "--center-freq", dest="center_freq", type=eng_float, default="3.56G",
        help="Set center_freq [default=%(default)r]")
    parser.add_argument(
        "-g", "--gain", dest="gain", type=eng_float, default="20.0",
        help="Set gain [default=%(default)r]")
    parser.add_argument(
        "-i", "--index", dest="index", type=eng_float, default="0.0",
        help="Set index [default=%(default)r]")
    parser.add_argument(
        "-s", "--samp-rate", dest="samp_rate", type=eng_float, default="24.0M",
        help="Set samp_rate [default=%(default)r]")
    return parser

def timing(self):    
    
    # Get user time
    print('Type in desired start time (min only)')
    user_start_time = (int(input()),)

    # Convert local time to sturct_time format
    local_time = time.time()
    user_time = time.localtime(local_time)
    
    # Create future time in struct_time format
    t = user_time[0:4]+user_start_time+(0,)+user_time[6:9]
    
    # Convert future time to seconds
    future_time = time.mktime(t)
    print('Start time in Sec: ', future_time)
    
    # Set start time delay to time difference between future and local time
    start_time = int(future_time - local_time)
    
    # Set start time, where start_time > 2.0
    self.uhd_usrp_source_0.set_start_time(uhd.time_spec(start_time))
   
    # Set to one radio next pps, initially
    self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec(0.0))
    curr_hw_time = self.uhd_usrp_source_0.get_time_last_pps()
    while curr_hw_time==self.uhd_usrp_source_0.get_time_last_pps():
        pass
    # Sleep for 50ms
    time.sleep(0.05)

    # Synchronize both radios time registers
    self.uhd_usrp_source_0.set_time_next_pps(uhd.time_spec_t(0.0))

    # Sleep for a couple seconds to make sure all usrp time registers latched and settled
    time.sleep(2)

    # Check the last pps time
    for ii in range(0,5):
        last_pps0 = self.uhd_usrp_source_0.get_time_last_pps()

        print("last_pps0 : %6.12f"%uhd.time_spec_t.get_real_secs(last_pps0))
      
        time.sleep(1.0)

    # For completion varification
    print(time.ctime())
    print('Processing time: ', time.process_time())
    
    return future_time

def stopRadio(self, future_time):

    runtime = int(input("Enter runtime: "))
    time.sleep(runtime)
    if time.time() == future_time + runtime:
        self.uhd_usrp_source_0.stop()


def main(top_block_cls=cir_rx8d,options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(alpha=options.alpha, center_freq=options.center_freq, gain=options.gain, index=options.index, samp_rate=options.samp_rate)

    future_time = timing(tb)
    tb.start()
    tb.show()
    #tb.stopRadio(future_time)

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
   main()
